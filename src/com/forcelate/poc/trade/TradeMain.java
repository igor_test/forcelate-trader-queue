package com.forcelate.poc.trade;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class TradeMain {
	private static final int THREADS_COUNT = 1;
	private static volatile boolean active = true;


	public static void main(String[] args) {
		BlockingQueue<Action> queue = new PriorityBlockingQueue<>();
//		queue = new LinkedBlockingQueue<>();

		List<Thread> processors = new ArrayList<>();
		// TODO shutdown Threads
		for (int i = 0; i < THREADS_COUNT; i++) {
			Thread processorThread = new Thread(() -> processQueue(queue), "Processor-" + i);
			processors.add(processorThread);
			processorThread.start();
		}

		//ScheduledExecutorService tradeThreadPool = Executors.newSingleThreadScheduledExecutor();
		//ScheduledFuture<?> scheduleMyTrade = scheduleAction(tradeThreadPool, queue, new MyTradesActionFactory(), 1000);
		//scheduleAction(Executors.newSingleThreadScheduledExecutor(), queue, new PlaceOrderActionFactory(), 5000);
		//scheduleAction(queue, new CancelOrderActionFactory(), 5000);
		
		//executor.shutdown(); // TODO
		
		/*new Thread() {
			public void run() {
				Helper.sleepSilent(10_000);
				System.out.println("Cancel My Trades");
				scheduleMyTrade.cancel(false);
				scheduleAction(tradeThreadPool, queue, new CancelOrderActionFactory(), 5000);
			};
		}.start();*/
		
		new Thread("Shutdown") {
			public void run() {
				Helper.sleepSilent(5_000);
				System.out.println("Shutdown");
				active = false;
				processors.forEach(Thread::interrupt);
			};
		}.start();
		
		new Thread("Size") {
			public void run() {
				int preSize = 0;
				while (active) {
					int size = queue.size();
					if (preSize != size) {
						System.out.println("\tQueue = " + size + " => " + queue);
						preSize = size;
					}
				}
			};
		}.start();
		
		// TODO shutdown
	}
	
	private static void processQueue(BlockingQueue<Action> queue) {
		try {
			while (active) {
				Action action = queue.take();
				System.out.println("Process " + action);
				action.execute();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private static ScheduledFuture<?> scheduleAction(ScheduledExecutorService scheduledThreadPool, BlockingQueue<Action> queue, ActionFactory actionFactory, int milis) {
		// TODO shutdown executors
		ScheduledFuture<?> scheduledFuture = scheduledThreadPool.scheduleWithFixedDelay(() -> {
			try {
				Action action = actionFactory.createAction();
				System.out.println("Add " + action);
				queue.put(action);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}, 0, milis, TimeUnit.MILLISECONDS);
		return scheduledFuture;
	}
}
