package com.forcelate.poc.trade;

public interface ActionFactory {
	Action createAction();
}
