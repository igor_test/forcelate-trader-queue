package com.forcelate.poc.trade;

public enum ActionType {
	CANCEL_ORDER,
	PLACE_ORDER,
	MY_TRADES,
	MY_ORDERS,
	TIM_MOLTER_BALANCE,
	MARGIN_POSITION,
	BALANCE_DETAILS,
	TRANSFER_BALANCE,
	ALL_TRADES,
	ORDER_BOOK,
	MY_CHARTS,
	SHUTDOWN;
}
