package com.forcelate.poc.trade;

public class Action implements Comparable<Action> {
	private ActionType type;
	private Runnable callback;

	public Action(ActionType type, Runnable callback) {
		this.type = type;
		this.callback = callback;
	}
	
	public void execute() {
		callback.run();
	}

	@Override
	public int compareTo(Action otherAction) {
		return this.type.compareTo(otherAction.type);
	}
	
	@Override
	public String toString() {
		return type.toString();
	}
}
