package com.forcelate.poc.trade;

import java.util.concurrent.atomic.AtomicInteger;

public class PlaceOrderActionFactory implements ActionFactory {

	private AtomicInteger counter = new AtomicInteger();

	@Override
	public Action createAction() {
		Action action = new Action(ActionType.PLACE_ORDER, () -> {
			int id = counter.incrementAndGet();
			System.out.println("\t\tPlace #" + id + " start");
			System.out.println("\t\tPlace #" + id + " done");
		});
		return action;
	}
}
