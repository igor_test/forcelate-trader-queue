package com.forcelate.poc.trade;

import java.util.concurrent.atomic.AtomicInteger;

public class MyTradesActionFactory implements ActionFactory {

	private AtomicInteger counter = new AtomicInteger();

	@Override
	public Action createAction() {
		Action action = new Action(ActionType.MY_TRADES, () -> {
			int id = counter.incrementAndGet();
			System.out.println("\t\tMy Trade #" + id + " start");
			System.out.println("\t\tMy Trade #" + id + " sleep (2s)");
			Helper.sleepSilent(2000);
			System.out.println("\t\tMy Trade #" + id + " done");
		});
		return action;
	}
}
