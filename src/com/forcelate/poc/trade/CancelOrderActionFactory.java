package com.forcelate.poc.trade;

import java.util.concurrent.atomic.AtomicInteger;

public class CancelOrderActionFactory implements ActionFactory {

	private AtomicInteger counter = new AtomicInteger();

	@Override
	public Action createAction() {
		Action action = new Action(ActionType.CANCEL_ORDER, () -> {
			int id = counter.incrementAndGet();
			System.out.println("\t\tCancel #" + id + " start");
			System.out.println("\t\tCancel #" + id + " done");
		});
		return action;
	}
}
