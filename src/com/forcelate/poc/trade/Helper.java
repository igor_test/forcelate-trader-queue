package com.forcelate.poc.trade;

public class Helper {
	public static void sleepSilent(long millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
